package com.mkoterx.demospringspecificationquerydsl;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoSpringSpecificationQuerydslApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoSpringSpecificationQuerydslApplication.class, args);
    }
}




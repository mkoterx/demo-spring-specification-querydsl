package com.mkoterx.demospringspecificationquerydsl;

import javax.persistence.*;

@Entity
@Table(name = "smart_phones")
class SmartPhone {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    long id;
    String manufacturer;
    String model;
    float memory;
    float internalStorage;
    float price;
    float screenSize;

    public SmartPhone(String manufacturer, String model, float memory, float internalStorage, float price, float screenSize) {
        this.manufacturer = manufacturer;
        this.model = model;
        this.memory = memory;
        this.internalStorage = internalStorage;
        this.price = price;
        this.screenSize = screenSize;
    }

    // JPA only
    public SmartPhone() {
    }

    public long getId() {
        return id;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public String getModel() {
        return model;
    }

    public float getMemory() {
        return memory;
    }

    public float getInternalStorage() {
        return internalStorage;
    }

    public float getPrice() {
        return price;
    }

    public float getScreenSize() {
        return screenSize;
    }
}

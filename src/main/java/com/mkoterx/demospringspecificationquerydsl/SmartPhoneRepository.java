package com.mkoterx.demospringspecificationquerydsl;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface SmartPhoneRepository extends JpaRepository<SmartPhone, Long>, JpaSpecificationExecutor {
}

package com.mkoterx.demospringspecificationquerydsl;

import java.util.List;
import java.util.Map;

class SmartPhoneFilterCriteria {

    List<String> manufacturers;
    List<Float> memories;
    List<String> internalStorages;
    Map<String, Float> priceRange;
    Map<String, Float> screenSizeRange;

    public void setInternalStorages(List<String> internalStorages) {
        this.internalStorages = internalStorages;
    }

    public void setManufacturers(List<String> manufacturers) {
        this.manufacturers = manufacturers;
    }

    public void setScreenSizeRange(Map<String, Float> screenSizeRange) {
        this.screenSizeRange = screenSizeRange;
    }

    public void setMemories(List<Float> memories) {
        this.memories = memories;
    }

    public void setPriceRange(Map<String, Float> priceRange) {
        this.priceRange = priceRange;
    }
}

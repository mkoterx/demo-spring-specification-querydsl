package com.mkoterx.demospringspecificationquerydsl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static org.springframework.data.jpa.domain.Specification.where;

@RestController
public class SmartPhoneController {


    @Autowired
    SmartPhoneRepository smartPhoneRepository;

    @PostMapping("smartphones/filter")
    public Collection<SmartPhone> getPeople(@RequestBody SmartPhoneFilterCriteria criteria) {

        Specification<SmartPhone> specManufacturer = (root, query, cb) -> {
            if (criteria.manufacturers == null || criteria.manufacturers.isEmpty())
                return cb.and(); // always true
            List<Predicate> predicates = new ArrayList<>();
            for (String selected : criteria.manufacturers) {
                predicates.add(cb.equal(root.get("manufacturer"), selected));
            }
            return cb.or(predicates.toArray(new Predicate[predicates.size()]));
        };
        Specification<SmartPhone> specStorage = (root, query, cb) -> {
            if (criteria.internalStorages == null || criteria.internalStorages.isEmpty())
                return cb.and(); // always true
            List<Predicate> predicates = new ArrayList<>();
            for (String selected : criteria.internalStorages) {
                predicates.add(cb.equal(root.get("internalStorage"), selected));
            }
            return cb.or(predicates.toArray(new Predicate[predicates.size()]));
        };
        Specification<SmartPhone> specMemory = (root, query, cb) -> {
            if (criteria.memories == null || criteria.memories.isEmpty())
                return cb.and(); // always true
            List<Predicate> predicates = new ArrayList<>();
            for (Float selected : criteria.memories) {
                predicates.add(cb.equal(root.get("memory"), selected));
            }
            return cb.or(predicates.toArray(new Predicate[predicates.size()]));
        };
        Specification<SmartPhone> specPriceRange = (root, query, cb) -> {
            if (criteria.priceRange == null) return cb.and(); // always true
            if (!criteria.priceRange.containsKey("from")) criteria.priceRange.put("from", 0F);
            if (!criteria.priceRange.containsKey("to")) criteria.priceRange.put("to", Float.MAX_VALUE);
            return cb.between(root.get("price"), criteria.priceRange.get("from"), criteria.priceRange.get("to"));
        };
        Specification<SmartPhone> specScreenSizeRange = (root, query, cb) -> {
            if (criteria.screenSizeRange == null) return cb.and(); // always true
            if (!criteria.screenSizeRange.containsKey("from")) criteria.screenSizeRange.put("from", 0F);
            if (!criteria.screenSizeRange.containsKey("to")) criteria.screenSizeRange.put("to", Float.MAX_VALUE);
            return cb.between(root.get("screenSize"), criteria.screenSizeRange.get("from"), criteria.screenSizeRange.get("to"));
        };
        Specification<SmartPhone> spec =
                where(specManufacturer)
                        .and(specScreenSizeRange)
                        .and(specMemory)
                        .and(specPriceRange)
                        .and(specStorage);
        return smartPhoneRepository.findAll(spec);
    }
}
